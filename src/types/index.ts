export * from './main.type';
export * from './socket.type';
export * from './game.type';
export * from './gamepad.type';
export * from './game-console.type';
